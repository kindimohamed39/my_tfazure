resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
  location = var.location
}


resource "azurerm_container_registry" "acr" {
  name     = var.acr_name
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  sku                      = "Basic"
  admin_enabled            = true
}


# ajouter des nouvelles ressources VNet, NSG, Subnets, VM)

resource "azurerm_network_security_group" "vnet1" {
  name                = var.vnet_security_group
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}



